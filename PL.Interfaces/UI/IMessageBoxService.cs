﻿
namespace PL.Interfaces.UI
{
    public interface IMessageBoxService
    {
        bool ShowMessage(string text, string caption);
    }
}