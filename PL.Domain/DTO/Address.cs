﻿namespace PL.Domain.DTO
{
    public class Address : BaseEntity
    {
        public string Country { get; set; }
        public string RegionCode { get; set; }
        public string Description { get; set; }
    }
}