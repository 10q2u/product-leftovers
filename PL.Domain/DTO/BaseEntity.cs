﻿using System;

namespace PL.Domain.DTO
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }
    }
}