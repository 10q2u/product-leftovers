﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PL.Domain.DTO
{
    public class Producer : BaseEntity
    {
        public string ClientRegId { get; set; }
        public string INN { get; set; }
        public string KPP { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }

        public Guid? AddressId { get; set; }
        [ForeignKey("AddressId")]
        public Address Address { get; set; }
        
    }
}