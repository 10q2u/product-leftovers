﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PL.Domain.DTO
{
    public class Product : BaseEntity
    {
        public string FullName { get; set; }
        public string AlcCode { get; set; }
        public string Capacity { get; set; }
        public string UnitType { get; set; }
        public string AlcVolume { get; set; }
        public string ProductVCode { get; set; }

        public Guid? ProducerId { get; set; }
        [ForeignKey("ProducerId")]
        public virtual Producer Producer { get; set; }
        
    }
}