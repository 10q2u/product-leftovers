﻿#nullable enable
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace PL.Domain.DTO
{
    public class Stock : BaseEntity
    {
        public string Quantity { get; set; }
        public string InformF1RegId { get; set; }
        public string InformF2RegId { get; set; }

        public Guid? ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
    }
}