﻿using System.Linq;
using System.Windows;
using System.Windows.Documents;
using PL.Domain.DTO;
using PL.Interfaces.DataAccess.Repositories;
using PL.ViewModels;
using PL.WpfPresentation.Repositories.Abstract;

namespace PL.WpfPresentation.Repositories.EntityFramework
{
    public class EFDocumentRendererRepository : IDocumentRendererRepository
    {
        private readonly IBaseRepository<Stock> _stockRepository;

        public EFDocumentRendererRepository(IBaseRepository<Stock> stockRepository)
        {
            _stockRepository = stockRepository;
        }

        public void Render(FlowDocument doc, object data)
        {
            if (!_stockRepository.GetAll().Any())
                return;

            TableRowGroup group = doc.FindName("rowsDetails") as TableRowGroup;
            Style styleCell = doc.Resources["BorderedCell"] as Style;

            var models = _stockRepository.GetAll().Select(x => new StockVm
                {
                    FullName = x.Product.FullName,
                    Quantity = x.Quantity,
                    InformF1RegId = x.InformF1RegId,
                    InformF2RegId = x.InformF2RegId,
                    AlcCode = x.Product.AlcCode,
                    Capacity = x.Product.Capacity,
                    ProductVCode = x.Product.ProductVCode
                }
            );

            foreach (StockVm item in models)
            {
                TableRow row = new TableRow();

                TableCell cell = new TableCell(new Paragraph(new Run(item.FullName)));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.Quantity)));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.AlcCode)));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.ProductVCode)));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.InformF1RegId)));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(
                    new Paragraph(new Run(item.InformF2RegId)));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run((item.Capacity))));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                group.Rows.Add(row);
            }
        }

        public void RenderFixedDocument(FixedDocument doc, object data)
        {
            if (!_stockRepository.GetAll().Any())
                return;

            TableRowGroup group = doc.FindName("rowsDetails") as TableRowGroup;
            Style styleCell = doc.Resources["BorderedCell"] as Style;

            var models = _stockRepository.GetAll().Select(x => new StockVm
                {
                    FullName = x.Product.FullName,
                    Quantity = x.Quantity,
                    InformF1RegId = x.InformF1RegId,
                    InformF2RegId = x.InformF2RegId,
                    AlcCode = x.Product.AlcCode,
                    Capacity = x.Product.Capacity,
                    ProductVCode = x.Product.ProductVCode
                }
            );

            foreach (StockVm item in models)
            {
                TableRow row = new TableRow();

                TableCell cell = new TableCell(new Paragraph(new Run(item.FullName)));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.Quantity)));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.AlcCode)));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.ProductVCode)));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run(item.InformF1RegId)));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(
                    new Paragraph(new Run(item.InformF2RegId)));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                cell = new TableCell(new Paragraph(new Run((item.Capacity))));
                cell.Style = styleCell;
                row.Cells.Add(cell);

                group.Rows.Add(row);
            }
        }
    }
}