﻿using System;
using System.Windows.Documents;

namespace PL.WpfPresentation.Repositories.Abstract
{
    public interface IDocumentRendererRepository
    {
        void Render(FlowDocument doc, Object data);
        void RenderFixedDocument(FixedDocument doc, Object data);
    }
}