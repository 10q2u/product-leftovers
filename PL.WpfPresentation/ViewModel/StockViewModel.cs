﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using MathCore.WPF.Commands;
using PL.Domain.DTO;
using PL.Interfaces.DataAccess.Repositories;
using PL.ViewModels;
using PL.WpfPresentation.Repositories.EntityFramework;
using PL.WpfPresentation.Views.Windows;

namespace PL.WpfPresentation.ViewModel
{
    public class StockViewModel : MathCore.WPF.ViewModels.ViewModel
    {
        private readonly IBaseRepository<Stock> _stockRepository;
        private readonly IBaseRepository<Product> _productRepository;

        public StockViewModel(IBaseRepository<Stock> stockRepository,
            IBaseRepository<Product> productRepository)
        {
            _stockRepository = stockRepository;
            _productRepository = productRepository;
            GetStocksFromDb();
        }

        public ICollectionView ProductView { get; set; }
        private ObservableCollection<StockVm> Stocks { get; set; }

        /// <summary>
        /// Получение списка товаров из Бд
        /// </summary>
        /// <returns>Stocks</returns>
        private void GetStocksFromDb()
        {
            var listingFromDb = _stockRepository.GetAll()
                .Select(x => new StockVm
                {
                    Id = x.Id,
                    Quantity = x.Quantity,
                    InformF1RegId = x.InformF1RegId,
                    InformF2RegId = x.InformF2RegId,
                    AlcCode = _productRepository.GetAll().FirstOrDefault(p => p.Id == x.ProductId)?.AlcCode,
                    FullName = _productRepository.GetAll().FirstOrDefault(p => p.Id == x.ProductId)?.FullName,
                    Capacity = _productRepository.GetAll().FirstOrDefault(p => p.Id == x.ProductId)?.Capacity,
                    ProductVCode = _productRepository.GetAll().FirstOrDefault(p => p.Id == x.ProductId)?.ProductVCode
                });

            if (!listingFromDb.Any())
                MessageBox.Show("Бд StockViewModel пуста");

            Stocks = new ObservableCollection<StockVm>(listingFromDb);
            ProductView = (CollectionView) CollectionViewSource.GetDefaultView(Stocks);

            ProductView.Filter = OnFilterTriggered;
        }

        #region SearchText : String - Поиск по TxtBox DataGrid

        /// <summary></summary>
        private string _searchText = string.Empty;

        /// <summary></summary> 
        public string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                OnPropertyChanged(nameof(SearchText));
                FilterData();
            }
        }

        #endregion

        #region ShowStockDbCommand : Command - Обновить лист

        /// <summary>Отобразить представление бд</summary>
        private ICommand _showStockViewCommand;

        /// <summary>Отобразить представление бд</summary>
        public ICommand ShowStockViewCommand => _showStockViewCommand
            ??= new LambdaCommand(GetStocksFromDb);

        #endregion

        #region SelectedItem : StockVm - Выбор продукта

        /// <summary>
        /// Выбор продукта по строке
        /// </summary>
        private StockVm _selectedItem;

        public StockVm SelectedItem
        {
            get => _selectedItem;
            set
            {
                if (value != _selectedItem)
                {
                    _selectedItem = value;
                    var stock = _stockRepository.GetByIdAsync(_selectedItem.Id).Result;

                    var stockEditorModel = new StockEditorViewModel(stock, _stockRepository, _productRepository);

                    var stockEditorWindow = new StockEditorWindow()
                    {
                        DataContext = stockEditorModel
                    };

                    if (stockEditorWindow.ShowDialog() != true)
                    {
                        var stockUpdate = _stockRepository.GetByIdAsync(stockEditorModel.SelectedProduct.Id).Result;

                        if (stockUpdate == null || stockEditorModel.SelectedProduct.ProductId == null)
                            return;

                        stockUpdate.Quantity = stockEditorModel.Quaintity;
                        stockUpdate.InformF1RegId = stockEditorModel.InformF1RegId;
                        stockUpdate.InformF2RegId = stockEditorModel.InformF2RegId;
                        stockUpdate.Product = _productRepository
                            .GetByIdAsync(stockEditorModel.SelectedProduct.Product.Id).Result;

                        _stockRepository.UpdateAsync(stockUpdate);

                        return;
                    }
                }
            }
        }

        #endregion

        #region AddNewStockCommand : Command - Добавление нового stock

        /// <summary>Добавление нового stock</summary>
        private ICommand _addNewStockCommand;

        /// <summary>Добавление нового stock</summary>
        public ICommand AddNewStockCommand => _addNewStockCommand
            ??= new LambdaCommand(OnAddNewStockCommandExecuted, CanAddNewStockCommandExecute);

        /// <summary>Проверка возможности выполнения - Добавление нового stock</summary>
        private bool CanAddNewStockCommandExecute() => true;

        /// <summary>Логика выполнения - Добавление нового stock</summary>
        private void OnAddNewStockCommandExecuted()
        {
            var stock = new Stock();

            var stockEditorModel = new StockEditorViewModel(stock, _stockRepository, _productRepository);

            var stockEditorWindow = new StockEditorWindow
            {
                DataContext = stockEditorModel
            };

            if (stockEditorWindow.ShowDialog() == true)
                return;

            if (stockEditorModel.Quaintity == null
                || stockEditorModel.InformF1RegId == null
                || stockEditorModel.InformF2RegId == null
                || stockEditorModel.SelectedProduct.Product.Id == null
            )
            {
                stockEditorWindow = new StockEditorWindow
                {
                    DataContext = stockEditorModel
                };
            }

            var product = _productRepository.GetByIdAsync(stockEditorModel.SelectedProduct.Product.Id).Result;

            stock.Id = Guid.NewGuid();
            stock.Quantity = stockEditorModel.Quaintity;
            stock.InformF1RegId = stockEditorModel.InformF1RegId;
            stock.InformF2RegId = stockEditorModel.InformF2RegId;
            if (product != null)
            {
                stock.ProductId = product.Id;
                stock.Product = product;
            }

            _stockRepository.AddAsync(stock);
            System.Windows.MessageBox.Show("Товар добавлен");

            GetStocksFromDb();
        }

        #endregion

        #region OnFilterTriggered : bool - Проверка на поля StockVm

        /// <summary>
        /// Проверка на поля StockVm
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private bool OnFilterTriggered(object item)
        {
            if (item is StockVm stockVm)
            {
                return stockVm.FullName.ToLower().Contains(SearchText.ToLower())
                       || stockVm.Quantity.ToLower().Contains(SearchText.ToLower())
                       || stockVm.AlcCode.ToLower().Contains(SearchText.ToLower())
                       || stockVm.Capacity.ToLower().Contains(SearchText.ToLower())
                       || stockVm.InformF1RegId.ToLower().Contains(SearchText.ToLower())
                       || stockVm.InformF2RegId.ToLower().Contains(SearchText.ToLower());
            }

            return false;
        }

        #endregion

        #region FilterData : void - обновление коллекции поиска

        /// <summary>
        /// 
        /// </summary>
        private void FilterData()
        {
            ProductView.Refresh();
        }

        #endregion

        #region PrintingDocumentCommand : Command - Превью печати Flow Document

        /// <summary></summary>
        private ICommand _printingDocumentCommand;

        /// <summary>Добавление нового stock</summary>
        public ICommand PrintingDocumentCommand => _printingDocumentCommand
            ??= new LambdaCommand(OnPrintingDocumentCommandExecuted, CanPrintingDocumentCommandExecute);

        /// <summary>Проверка возможности выполнения - Добавление нового stock</summary>
        private bool CanPrintingDocumentCommandExecute() => true;

        /// <summary>Логика выполнения - Добавление нового stock</summary>
        private void OnPrintingDocumentCommandExecuted()
        {
            PrintPreviewWindow previewWindow =
                new PrintPreviewWindow("OrderDocument.xaml", "stockks", new EFDocumentRendererRepository(_stockRepository));
            previewWindow.ShowInTaskbar = false;
            previewWindow.ShowDialog();
        }

        #endregion
        
        #region PrintDialogDocumentCommand : Command - Превью печати из модуля PrintDialog

        /// <summary></summary>
        private ICommand _printDialogDocumentCommand;

        /// <summary>Добавление нового stock</summary>
        public ICommand PrintDialogDocumentCommand => _printingDocumentCommand
            ??= new LambdaCommand(OnPrintDialogDocumentCommandExecuted, CanPrintDialogDocumentCommandExecute);

        /// <summary>Проверка возможности выполнения - Добавление нового stock</summary>
        private bool CanPrintDialogDocumentCommandExecute() => true;

        /// <summary>Логика выполнения - Добавление нового stock</summary>
        private void OnPrintDialogDocumentCommandExecuted()
        {
            PrintPreviewWindow previewWindow =
                new PrintPreviewWindow("OrderDocument.xaml", "stockks", new EFDocumentRendererRepository(_stockRepository));
            previewWindow.ShowInTaskbar = false;
            previewWindow.ShowDialog();
        }

        #endregion
    }
}