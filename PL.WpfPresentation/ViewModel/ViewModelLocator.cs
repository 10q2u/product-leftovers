﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PL.Infrastructures;
using PL.Infrastructures.Repositories;
using PL.Interfaces.DataAccess.Repositories;

namespace PL.WpfPresentation.ViewModel
{
    public class ViewModelLocator
    {
        private static ServiceProvider _serviceProvider;

        public ViewModelLocator()
        {
            ServiceCollection services = new ServiceCollection();

            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlite("Data Source =Leftovers.sqlite");
                // options.UseSqlite(Configuration.GetConnectionString("Database") );
            });
            
            services.AddSingleton(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            
            services.AddSingleton<MainWindowsVm>();
            services.AddSingleton<ParserXmlViewModel>();
            services.AddSingleton<StockViewModel>();
            services.AddSingleton<StockEditorViewModel>();
            services.AddSingleton<TestUserControlViewModel>();
         
            _serviceProvider = services.BuildServiceProvider();
        }
        
        public static MainWindowsVm MainWindowsVm => (MainWindowsVm) _serviceProvider.GetService(typeof(MainWindowsVm));
        public static ParserXmlViewModel ParserXmlViewModel => (ParserXmlViewModel) _serviceProvider.GetService(typeof(ParserXmlViewModel));
        public static StockViewModel StockViewModel => (StockViewModel) _serviceProvider.GetService(typeof(StockViewModel));
        public static StockEditorViewModel StockEditorViewModel => (StockEditorViewModel) _serviceProvider.GetService(typeof(StockEditorViewModel));
        public static TestUserControlViewModel TestUserControlViewModel => (TestUserControlViewModel) _serviceProvider.GetService(typeof(TestUserControlViewModel));
    }
}