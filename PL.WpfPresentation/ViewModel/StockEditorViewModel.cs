﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using MathCore.WPF.Commands;
using PL.Domain.DTO;
using PL.Interfaces.DataAccess.Repositories;
using PL.ViewModels;

namespace PL.WpfPresentation.ViewModel
{
    public class StockEditorViewModel : MathCore.WPF.ViewModels.ViewModel
    {
        private readonly IBaseRepository<Stock> _stockRepository;
        private readonly IBaseRepository<Product> _productRepository;

        public StockEditorViewModel(Stock stock,
            IBaseRepository<Stock> stockRepository,
            IBaseRepository<Product> productRepository)
        {
            _stockRepository = stockRepository;
            _productRepository = productRepository;
            Id = stock.Id;
            Quaintity = stock.Quantity;
            InformF1RegId = stock.InformF1RegId;
            InformF2RegId = stock.InformF2RegId;
            Product = stock.Product;
            if (stock.ProductId != null) ProductId = (Guid) stock.ProductId;
            GetStocksFromDb();
        }
        
        public List<StockVm> StockVms { get; set; }

        #region GetStocksFromDb : void - Получение списка товаров

        /// <summary>
        /// Получение списка товаров из Бд
        /// </summary>
        /// <returns>Stocks</returns>
        private void GetStocksFromDb()
        {
            var listingFromDb = _stockRepository.GetAll()
                .Select(x => new StockVm
                {
                    Id = x.Id,
                    Quantity = x.Quantity,
                    InformF1RegId = x.InformF1RegId,
                    InformF2RegId = x.InformF2RegId,

                    FullName = _productRepository.GetAll().FirstOrDefault(p => p.Id == x.ProductId)?.FullName,
                    AlcCode = _productRepository.GetAll().FirstOrDefault(p => p.Id == x.ProductId)?.AlcCode,
                    Capacity = _productRepository.GetAll().FirstOrDefault(p => p.Id == x.ProductId)?.Capacity,
                    ProductVCode = _productRepository.GetAll().FirstOrDefault(p => p.Id == x.ProductId)?.ProductVCode,
                    ProductId = _productRepository.GetAll().FirstOrDefault(p => p.Id == x.ProductId)?.Id
                });
                

            if (!listingFromDb.Any())
                MessageBox.Show("Бд StockEditorViewModel пуста");

            Stocks = new ObservableCollection<StockVm>(listingFromDb);
            StockVms = new List<StockVm>(listingFromDb);
        }

        #endregion

        #region ObservableCollection : StockVm - коллекция

        /// <summary>
        /// Коллекция Stocks
        /// </summary>
        private ObservableCollection<StockVm> _stocks;

        public ObservableCollection<StockVm> Stocks
        {
            get => _stocks;
            set
            {
                _stocks = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Id : Guid - prop Stock.Id
        
        /// <summary>Id Stock</summary>
        private Guid Id { get; }

        #endregion

        #region Quaintity : String - Quiantity

        /// <summary>Quantity</summary>
        private string _Quaintity;

        /// <summary>Qiantity</summary>
        public string Quaintity
        {
            get => _Quaintity;
            set => Set(ref _Quaintity, value);
        }

        #endregion

        #region InformF1RegId : String - InformF1RegId

        /// <summary>InformF1RegId</summary>
        private string _InformF1RegId;

        /// <summary>InformF1RegId</summary>
        public string InformF1RegId
        {
            get => _InformF1RegId;
            set => Set(ref _InformF1RegId, value);
        }

        #endregion

        #region InformF2RegId : String - InformF2RegId

        /// <summary>InformF2RegId</summary>
        private string _InformF2RegId;

        /// <summary>InformF2RegId</summary>
        public string InformF2RegId
        {
            get => _InformF2RegId;
            set => Set(ref _InformF2RegId, value);
        }

        #endregion

        #region ProductId : Guid - ProductId

        /// <summary>ProductId</summary>
        private Guid _ProductId;

        /// <summary>ProductId</summary>
        public Guid ProductId
        {
            get => _ProductId;
            set => Set(ref _ProductId, value);
        }

        #endregion

        #region Product : Product - объект товара

        /// <summary>Установление товара</summary>
        private Product _product;

        /// <summary>Товар</summary>
        public Product Product
        {
            get => _product;
            set => Set(ref _product, value);
        }

        #endregion
        
        #region SelectedProduct : Stock - Выбор товара

        /// <summary>Установление товара</summary>
        private StockVm _SelectedProduct;

        /// <summary>Товар</summary>
        public StockVm SelectedProduct
        {
            get => _SelectedProduct;
            set => _SelectedProduct = value;
        }

        #endregion

        #region RemoveCommand : ICommand - Удаление записи бд

        /// <summary>
        /// Удаление записи по БД
        /// </summary>
        private ICommand _removeCommand;

        public ICommand RemoveCommand
        {
            get
            {
                return _removeCommand ??= new LambdaCommand(obj =>
                {
                    var stock = _stockRepository.GetAll().FirstOrDefault(x => x.Id == Id);
                    _stockRepository.DeleteAsync(stock);
                    GetStocksFromDb();
                });
            }
        }

        #endregion
    }
}