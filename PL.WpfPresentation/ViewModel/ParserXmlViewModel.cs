﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using MathCore.WPF.Commands;
using Microsoft.Win32;
using PL.Common.HelperForXml;
using PL.Domain.DTO;
using PL.Interfaces.DataAccess.Repositories;
using PL.ViewModels;

namespace PL.WpfPresentation.ViewModel
{
    public class ParserXmlViewModel : MathCore.WPF.ViewModels.ViewModel
    {
        private static string _filePath = String.Empty;
        private readonly IBaseRepository<Stock> _stockRepository;

        public ParserXmlViewModel(IBaseRepository<Stock> stockRepository)
        {
            _stockRepository = stockRepository;
        }
        
        
        /// <summary>
        /// Коллекция Stocks
        /// </summary>
        private ObservableCollection<StockVm> _stocks;
        public ObservableCollection<StockVm> Stocks
        {
            get => _stocks;
            set
            {
                _stocks = value;
                OnPropertyChanged();
            }
        }
        
        
        /// <summary>
        /// Парсинг XML
        /// </summary>
        private ICommand _outStock;
        public ICommand OutStock
        {
            get
            {
                return _outStock ??= new LambdaCommand(obj =>
                {
                    var openFileDialog = new OpenFileDialog();
                    if (openFileDialog.ShowDialog() == true)
                        _filePath = openFileDialog.FileName;

                    var deserializeXml = DeserializeFromXml.DeserializeXml(_filePath);

                    if (!deserializeXml.Any())
                        return;
                        
                    var stockList = deserializeXml
                        .Select(x => new StockVm
                        {
                            ProductId = x.Product.Id,
                            Quantity = x.Quantity,
                            InformF1RegId = x.InformF1RegId,
                            InformF2RegId = x.InformF2RegId,
                            AlcCode = x.Product.AlcCode,
                            FullName = x.Product.Producer.ShortName,
                            Capacity = x.Product.Capacity,
                            ProductVCode = x.Product.ProductVCode
                        });

                    _stockRepository.AddRangeAsync(deserializeXml);

                    Stocks = new ObservableCollection<StockVm>(stockList);
                });
            }
        }

    }
}