﻿using System.Windows.Input;
using MathCore.WPF.Commands;

namespace PL.WpfPresentation.ViewModel
{
    public class MainWindowsVm : MathCore.WPF.ViewModels.ViewModel
    {
        #region CurrentModel : ViewModel - Текущая дочерняя модель-представления

        /// <summary>Текущая дочерняя модель-представления</summary>
        private MathCore.WPF.ViewModels.ViewModel _CurrentModel;

        /// <summary>Текущая дочерняя модель-представления</summary>
        public MathCore.WPF.ViewModels.ViewModel CurrentModel
        {
            get => _CurrentModel;
            private set => Set(ref _CurrentModel, value);
        }

        #endregion

        #region Command ShowStockXmlViewCommand - Отобразить представление Xml

        /// <summary>Отобразить представление xml</summary>
        private ICommand _ShowStockXmlViewCommand;

        /// <summary>Отобразить представление xml</summary>
        public ICommand ShowStockXmlViewCommand => _ShowStockXmlViewCommand
            ??= new LambdaCommand(OnShowStockXmlViewCommandExecuted, CanShowStockXmlViewCommandExecute);

        private bool CanShowStockXmlViewCommandExecute() => true;
        private void OnShowStockXmlViewCommandExecuted()
        {
            CurrentModel = ViewModelLocator.ParserXmlViewModel;
        }

        #endregion
        
        #region Command ShowStockDbViewCommand - Отобразить представление из БД

        /// <summary>Отобразить представление бд</summary>
        private ICommand _ShowStockDbViewCommand;

        /// <summary>Отобразить представление бд</summary>
        public ICommand ShowStockDbViewCommand => _ShowStockDbViewCommand
            ??= new LambdaCommand(OnShowStockDbViewCommandExecuted, CanShowStockDbViewCommandExecute);

        private bool CanShowStockDbViewCommandExecute() => true;
        private void OnShowStockDbViewCommandExecuted()
        {
            CurrentModel = ViewModelLocator.StockViewModel;
        }

        #endregion
    }
}