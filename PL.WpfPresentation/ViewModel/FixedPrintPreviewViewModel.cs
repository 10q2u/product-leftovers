﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using PL.Domain.DTO;
using PL.Interfaces.DataAccess.Repositories;
using PL.ViewModels;

namespace PL.WpfPresentation.ViewModel
{
    public class FixedPrintPreviewViewModel : MathCore.WPF.ViewModels.ViewModel
    {
        
        private readonly IBaseRepository<Stock> _stockRepository;
        private readonly IBaseRepository<Product> _productRepository;

        public FixedPrintPreviewViewModel(IBaseRepository<Stock> stockRepository, IBaseRepository<Product> productRepository)
        {
            _stockRepository = stockRepository;
            _productRepository = productRepository;
            GetStocksFromDb();
        }

        private ObservableCollection<StockVm> _stock;

        public ObservableCollection<StockVm> Stocks
        {
            get => _stock;
            set => Set(ref _stock, value);
        }

        /// <summary>
        /// Получение списка товаров из Бд
        /// </summary>
        /// <returns>Stocks</returns>
        private void GetStocksFromDb()
        {
            var listingFromDb = _stockRepository.GetAll()
                .Select(x => new StockVm
                {
                    Id = x.Id,
                    Quantity = x.Quantity,
                    InformF1RegId = x.InformF1RegId,
                    InformF2RegId = x.InformF2RegId,
                    AlcCode = _productRepository.GetAll().FirstOrDefault(p => p.Id == x.ProductId)?.AlcCode,
                    FullName = _productRepository.GetAll().FirstOrDefault(p => p.Id == x.ProductId)?.FullName,
                    Capacity = _productRepository.GetAll().FirstOrDefault(p => p.Id == x.ProductId)?.Capacity,
                    ProductVCode = _productRepository.GetAll().FirstOrDefault(p => p.Id == x.ProductId)?.ProductVCode
                });

            if (!listingFromDb.Any())
                MessageBox.Show("Бд StockViewModel пуста");

            Stocks = new ObservableCollection<StockVm>(listingFromDb);
        }
    }
}