﻿using System;

namespace PL.ViewModels
{
    public class ProducerVm
    {
        public Guid Id { get; set; }
        public string ClientRegId { get; set; }
        public string INN { get; set; }
        public string KPP { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }

        public virtual AddressVm AddressId { get; set; }
    }
}