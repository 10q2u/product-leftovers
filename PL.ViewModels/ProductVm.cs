﻿using System;

namespace PL.ViewModels
{
    public class ProductVm
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public string AlcCode { get; set; }
        public string Capacity { get; set; }
        public string UnitType { get; set; }
        public string AlcVolume { get; set; }
        public string ProductVCode { get; set; }
        
        public virtual ProducerVm ProducerId { get; set; }
    }
}