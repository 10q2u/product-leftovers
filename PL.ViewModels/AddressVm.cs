﻿using System;

namespace PL.ViewModels
{
    public class AddressVm
    {
        private Guid Id { get; set; }
        public string Country { get; set; }
        public string RegionCode { get; set; }
        public string Description { get; set; }
    }
}