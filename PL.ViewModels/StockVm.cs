﻿using System;

namespace PL.ViewModels
{
    public class StockVm
    {
        public Guid Id { get; set; }
        public Guid? ProductId { get; set; }
        public string Quantity { get; set; }
        public string InformF1RegId { get; set; }
        public string InformF2RegId { get; set; }
        public string AlcCode { get; set; }
        public string FullName { get; set; }
        public string Capacity { get; set; }
        public string ProductVCode { get; set; }

        public virtual ProductVm Product { get; set; }
    }
}