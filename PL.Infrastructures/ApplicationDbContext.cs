﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using PL.Domain.DTO;

namespace PL.Infrastructures
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Producer> Producers { get; set; }
        public DbSet<Address> Addresses { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlite("Data Source =Leftovers.sqlite",
        //        option => { option.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName); });
        //    base.OnConfiguring(optionsBuilder);
        //}
    }
}