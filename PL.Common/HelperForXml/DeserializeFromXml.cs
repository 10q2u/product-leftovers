﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using PL.Domain.DTO;
using PL.Interfaces.DataAccess.Repositories;
using PL.ViewModels;

namespace PL.Common.HelperForXml
{
    public static class DeserializeFromXml
    {
        private static readonly XNamespace _ns = "http://fsrar.ru/WEGAIS/WB_DOC_SINGLE_01";
        private static readonly XNamespace _rst = "http://fsrar.ru/WEGAIS/ReplyRests_v2";
        private static readonly XNamespace _pref = "http://fsrar.ru/WEGAIS/ProductRef_v2";
        private static readonly XNamespace _oref = "http://fsrar.ru/WEGAIS/ClientRef_v2";

        /// <summary>
        /// Получения объектов
        /// </summary>
        /// <param name="xElement"></param>
        /// <returns>Список всех объектов Stock</returns>
        public static List<Stock> DeserializeXml(string filePath)
        {
            List<Stock> stockLists = new List<Stock>();

            if (string.IsNullOrEmpty(filePath))
            {
                return new List<Stock>();
            }

            XDocument xDocument = XDocument.Load(filePath);

            if (!xDocument.Descendants(_rst + "StockPosition").Any())
            {
                return new List<Stock>();
            }

            foreach (var item in xDocument.Descendants(_rst + "StockPosition"))
            {
                Stock stockObj = new Stock();

                var stock = GetStockFromXmlElement(item);

                var productTag = item.Descendants(_rst + "Product").FirstOrDefault();
                var product = GetProductFromXmlElement(productTag);

                var producerTag = productTag.Descendants(_pref + "Producer").FirstOrDefault();
                var producer = GetProducerFromXmlElement(producerTag);

                // var addressTag = producerTag.Descendants(_oref + "address").FirstOrDefault();
                // var address = GetAddressFromXmlElement(addressTag);

                stockObj = new Stock
                {
                    Id = stock.Id,
                    Quantity = stock.Quantity,
                    InformF1RegId = stock.InformF1RegId,
                    InformF2RegId = stock.InformF2RegId,
                    ProductId = product.Id,
                    Product = new Product
                    {
                        Id = product.Id,
                        FullName = product.FullName,
                        AlcCode = product.AlcCode,
                        Capacity = product.Capacity,
                        UnitType = product.UnitType,
                        AlcVolume = product.AlcVolume,
                        ProductVCode = product.ProductVCode,
                        ProducerId = producer.Id,
                        Producer = new Producer
                        {
                            Id = producer.Id,
                            ClientRegId = producer.ClientRegId,
                            INN = producer.INN,
                            KPP = producer.KPP,
                            FullName = producer.FullName,
                            ShortName = producer.ShortName
                        }
                    }
                };
                stockLists.Add(stockObj);
            }

            return stockLists;
        }

        /// <summary>
        /// Получения Склада
        /// </summary>
        /// <param name="xElement"></param>
        /// <returns>Объект Stock</returns>
        private static Stock GetStockFromXmlElement(XElement xElement)
        {
            var stock = new Stock
            {
                Id = Guid.NewGuid(),
                Quantity = xElement.Element(_rst + "Quantity").Value,
                InformF1RegId = xElement.Element(_rst + "InformF1RegId").Value ?? String.Empty,
                InformF2RegId = xElement.Element(_rst + "InformF2RegId").Value ?? String.Empty
            };

            return stock;
        }

        /// <summary>
        /// Получения Продукта
        /// </summary>
        /// <param name="xElement"></param>
        /// <returns>Объект Product</returns>
        private static Product GetProductFromXmlElement(XElement xElement)
        {
            var product = new Product
            {
                Id = Guid.NewGuid(),
                FullName = xElement.Element(_pref + "FullName").Value,
                AlcCode = xElement.Element(_pref + "AlcCode").Value,
                Capacity = xElement.Element(_pref + "Capacity").Value,
                UnitType = xElement.Element(_pref + "UnitType").Value,
                AlcVolume = xElement.Element(_pref + "AlcVolume").Value,
                ProductVCode = xElement.Element(_pref + "ProductVCode").Value
            };

            return product;
        }

        /// <summary>
        /// Получения Производителя
        /// </summary>
        /// <param name="xElement"></param>
        /// <returns>Объект Producer</returns>
        private static Producer GetProducerFromXmlElement(XElement xElement)
        {
            var producer = new Producer();
            if (xElement.Descendants(_oref + "UL").Any())
            {
                producer = xElement.Descendants(_oref + "UL")
                    .Where(x => !x.IsEmpty)
                    .Select(x => new Producer
                    {
                        Id = Guid.NewGuid(),
                        ClientRegId = x.Element(_oref + "ClientRegId").Value,
                        INN = x.Element(_oref + "INN").Value,
                        KPP = x.Element(_oref + "KPP").Value,
                        FullName = x.Element(_oref + "FullName").Value,
                        ShortName = x.Element(_oref + "ShortName").Value
                    }).FirstOrDefault();
            }

            else if (xElement.Descendants(_oref + "TS").Any())
            {
                producer = xElement.Descendants(_oref + "TS").Where(x => !x.IsEmpty)
                    .Select(x => new Producer
                    {
                        Id = Guid.NewGuid(),
                        ClientRegId = x.Element(_oref + "ClientRegId").Value,
                        FullName = x.Element(_oref + "FullName").Value,
                        ShortName = x.Element(_oref + "ShortName").Value
                    }).FirstOrDefault();
            }

            else if (xElement.Descendants(_oref + "FO").Any())
            {
                producer = xElement.Descendants(_oref + "FO")
                    .Select(x => new Producer
                    {
                        Id = Guid.NewGuid(),
                        ClientRegId = x.Element(_oref + "ClientRegId").Value,
                        FullName = x.Element(_oref + "FullName").Value,
                        ShortName = x.Element(_oref + "ShortName").Value
                    }).FirstOrDefault();
            }

            return producer;
        }

        /// <summary>
        /// Получения Адреса
        /// </summary>
        /// <param name="xElement"></param>
        /// <returns>Объект Address</returns>
        private static Address GetAddressFromXmlElement(XElement xElement)
        {
            var address = new Address();

            if (xElement.Descendants(_oref + "TS").Any()
                || xElement.Descendants(_oref + "FO").Any()
                || xElement.Descendants(_oref + "UL").Any())
            {
                address = xElement.Descendants(_oref + "address")
                    .Select(x => new Address
                    {
                        Id = Guid.NewGuid(),
                        Country = x.Element(_oref + "Country").Value,
                        RegionCode = x.Element(_oref + "RegionCode").Value ?? " ",
                        Description = x.Element(_oref + "description").Value
                    }).FirstOrDefault();
            }

            return address;
        }
    }
}